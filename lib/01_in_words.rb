class Fixnum
  NUMBERS = {
    0 => "zero",
    1 => "one",
    2 => "two",
    3 => "three",
    4 => "four",
    5 => "five",
    6 => "six",
    7 => "seven",
    8 => "eight",
    9 => "nine",
    10 => "ten",
    11 => "eleven",
    12 => "twelve",
    13 => "thirteen",
    14 => "fourteen",
    15 => "fifteen",
    16 => "sixteen",
    17 => "seventeen",
    18 => "eighteen",
    19 => "nineteen",
    20 => "twenty",
    30 => "thirty",
    40 => "forty",
    50 => "fifty",
    60 => "sixty",
    70 => "seventy",
    80 => "eighty",
    90 => "ninety",
    100 => "hundred",
    1_000 => "thousand",
    1_000_000 => "million",
    1_000_000_000 => "billion",
    1_000_000_000_000 => "trillion",
  }

  def in_words
    if self < 20
      NUMBERS[self]
    elsif self < 100
      double_digits(self)
    else
      larger_digits(self)
    end
  end

  def larger_digits(num)
    large = large_number(num)
    digits_words = (num / large).in_words + " " + NUMBERS[large]
    remainder = num % large

    if remainder != 0
      digits_words + " " + remainder.in_words
    else
      digits_words
    end
  end

  def double_digits(num)
    double_digits = NUMBERS[num / 10 * 10]

    if num % 10 != 0
      double_digits + " " + (num % 10).in_words
    else
      double_digits
    end
  end

  def large_number(num)
    NUMBERS.keys.select { |key| num >= key }.last
  end
end
